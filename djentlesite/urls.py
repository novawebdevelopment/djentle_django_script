from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djentlesite.views.home', name='home'),
    # url(r'^djentlesite/', include('djentlesite.foo.urls')),
    url(r'^$', TemplateView.as_view(template_name='home.html')),
    url(r'^hello/$', TemplateView.as_view(template_name='helloworld.html')),
    url(r'^contact/$', views.ContactView.as_view()),
    url(r'^contactconfirm/$', TemplateView.as_view(template_name='contactconfirm.html')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^blogapp/', include('blogapp.urls')),
)
