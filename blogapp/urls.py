from django.conf.urls import patterns, url

from blogapp import views

urlpatterns = patterns('',
    url(r'^postlist/$', views.PostListView.as_view()),
    url(r'^posts/(?P<pk>\d+)/$', views.PostView.as_view()),
    url(r'^posts/(?P<pk>\d+)/edit/$', views.PostEditView.as_view()),
    url(r'^posts/add/$', views.PostAddView.as_view()),
    url(r'^posts/(?P<pk>\d+)/delete/$', views.PostDeleteView.as_view()),
)
